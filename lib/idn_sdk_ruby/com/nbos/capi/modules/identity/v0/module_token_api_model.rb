module IdnSdkRuby
	module Com
		module Nbos
			module Capi
				module Modules
					module Identity
						module V0
							class ModuleTokenApiModel < IdnSdkRuby::Com::Nbos::Capi::Modules::Identity::V0::BasicActiveModel
								attr_accessor :username, :clientId, :tokenType, :token, 
															:expiration, :expired, :tenantId, :modules, 
															:message, :uuid, :authorities

								def initialize(parsed_response = nil)
									if !parsed_response.nil?
										@username = parsed_response['username']
										@clientId = parsed_response['clientId']
										@tokenType = parsed_response['tokenType']
										@token = parsed_response['token']
										@expiration = parsed_response['expiration']
										@expired = parsed_response['expired']
										@tenantId = parsed_response['tenantId']
										@uuid = parsed_response['member']['uuid'] if parsed_response['member'].present?
										@modules = add_modules(parsed_response['modules']) if parsed_response['modules'].present?
										@authorities = add_authorities(parsed_response['authorities']) if parsed_response['authorities'].present?
									end
								end

								def add_modules(modules)
									@modules = []
									if modules.size > 0
										modules.each do |m|
											mod = IdnSdkRuby::Com::Nbos::Capi::Modules::Core::V0::ModuleApiModel.new(m["uuid"], m["name"])
											@modules << mod
										end
									end
								end

								def get_modules
									if self.modules.present?
										ms = []
										self.modules.each do |m|
											ms << m["name"]
										end
										ms
									else
										[]
									end
								end

								def add_authorities(authorities)
									@authorities = []
									if authorities.size > 0
										authorities.each do |at|
											auth = IdnSdkRuby::Com::Nbos::Capi::Modules::Core::V0::AuthorityApiModel.new(at["uAuthorityName"], at["displayName"], at["description"])
											@authorities << auth
										end
									end
								end

								def get_authorities
									if self.authorities.present?
										ms = []
										self.authorities.each do |m|
											ms << m["uAuthorityName"]
										end
										ms
									else
										[]
									end
								end

								def is_member?
									if self.uuid.present?
										true
									else
										false
									end
								end

								def has_authorities?
									if self.authorities.present?
										true
									else
										false
									end
								end

								def add_errors(json_response)
									json_response["errors"].each do |e|
										property_name = e['propertyName']
										msg = e['message']
										self.errors[property_name] << msg
									end
								end

								def add_messages(json_response)
									if json_response["message"].present?
										@message = json_response["message"]
									elsif json_response["error"].present?
										@message = json_response["error"]
									end

								end

								def as_json(options={})
									{
											username: @username,
											clientId: @clientId,
											tokenType: @tokenType,
											token: @token,
											expiration: @expiration,
											expired: @expired,
											tenantId: @tenantId,
											modules: @modules,
											uuid: @uuid
									}
								end

								def to_json(*options)
									as_json(*options).to_json(*options)
								end

								def to_s
									to_json
								end
							end
						end
					end
				end
			end
		end
	end
end