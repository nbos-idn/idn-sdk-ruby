require 'spec_helper'

describe IdnSdkRuby::Com::Nbos::Capi::Modules::Identity::V0::IdentityApi	 do

  before(:each) do
    app_context = double('IdsRailsApiContext')
    token_api_model = double('TokenApiModel')
    allow(app_context).to receive(:getClientCredentials){{"client_id"=>"sample-app-client",
                                                          "client_secret"=>"sample-app-secret"}}
    allow(app_context).to receive(:getName) {'app'}
    allow(app_context).to receive(:init)
    allow(app_context).to receive(:name){'app'}
    allow(app_context).to receive(:getHost).with(anything()){'http://api.qa1.nbos.io'}
    allow(app_context).to receive(:setClientToken).with(anything()){anything()}
    allow(token_api_model).to receive(:getAccess_token){"access_token"}
    allow(app_context).to receive(:getClientToken){token_api_model}
    allow(app_context).to receive(:getUserToken).with("identity"){"identitytoken"}
    allow(app_context).to receive(:setUserToken).with("identity", anything()){"identitytoken"}
    IdnSdkRuby::Com::Nbos::Capi::Api::V0::IdnSDK.init(app_context)
  end

  let(:args){{"username"=>"jagadish2","password"=>"654321","email"=>"jagadishwerb+1@wavelabs.in",
              "firstName"=>"jane", "lastName"=>"doe"}}
  let(:member_api_model_class){IdnSdkRuby::Com::Nbos::Capi::Modules::Identity::V0::MemberApiModel}
  let(:identity_api){IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::Ids.getModuleApi("identity")}
  let(:test_uuid){"MBR:test-uuid"}
  let(:login_model){IdnSdkRuby::Com::Nbos::Capi::Modules::Identity::V0::LoginModel.new("jagadish2", "123456")}
  let(:password_api_model){IdnSdkRuby::Com::Nbos::Capi::Modules::Identity::V0::UpdatePasswordApiModel.new("123456","654321")}

  it 'should allow User to signup' do
    response = identity_api.signup(args)
    expect(response[:data]).to           be_a_kind_of(member_api_model_class)
    expect(response[:data].email).to     eq(args["email"])
    expect(response[:data].firstName).to eq(args["firstName"])
    expect(response[:data].lastName).to  eq(args["lastName"])
  end

  it 'should allow User to Login' do
    response = identity_api.login(login_model)
    expect(response[:data]).to be_a_kind_of(member_api_model_class)
  end

  it 'should allow User to update Details' do
    response = identity_api.updateMemberDetails(test_uuid, args)
    expect(response[:data]).to          be_a_kind_of(member_api_model_class)
    expect(response[:data].email).to     eq(args["email"])
    expect(response[:data].firstName).to eq(args["firstName"])
    expect(response[:data].lastName).to  eq(args["lastName"])
  end

  it 'should allow User to update Password' do
    response = identity_api.updateCredentials(password_api_model)
    expect(response[:data]).to          be_a_kind_of(password_api_model.class)
    expect(response[:data].message).to  eq("password.changed")

  end

  it 'should allow User to logout' do
    response = identity_api.logout
    expect(response[:data]).to be_a_kind_of(login_model.class)
    expect(response[:data].message).to  eq("member.logout.success")
  end


  it 'should allow User to Connect Through social logins' do
    omni_hash = {credentials:{expires_at: 1494312526, token:"test::omniauth::token"}}
    response = identity_api.connect(omni_hash,'facebook')
    expect(response[:data]).to                         be_a_kind_of(member_api_model_class)
    expect(response[:data].isExternal).to              be(true)
    expect(response[:data].socialAccounts.present?).to be true
  end

end