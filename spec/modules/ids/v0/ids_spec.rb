require 'spec_helper'

describe IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::Ids do

  it 'should Register Module Api' do
    app_context = double('IdsRailsApiContext')
    allow(app_context).to receive(:getClientCredentials){{"client_id"=>"sample-app-client",
                                                            "client_secret"=>"sample-app-secret"}}
    allow(app_context).to receive(:getName) {'app'}
    allow(app_context).to receive(:init)
    allow(app_context).to receive(:name){'app'}
    allow(app_context).to receive(:getHost).with(anything()){'http://api.qa1.nbos.io'}
    allow(app_context).to receive(:setClientToken).with(anything()){anything()}
    IdnSdkRuby::Com::Nbos::Capi::Api::V0::IdnSDK.init(app_context)
    expect(IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::Ids.getModuleApi('app')).to be_a_kind_of IdnSdkRuby::Com::Nbos::Capi::Api::V0::NetworkApi
  end
  # for media
  #IdnSdkRuby::Com::Nbos::Capi::Modules::Media::V0::MediaApi
end