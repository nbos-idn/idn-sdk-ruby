require 'spec_helper'
require "idn_sdk_ruby/com/nbos/capi/modules/ids/v0/ids_api"
require "idn_sdk_ruby/com/nbos/capi/modules/ids/v0/ids_remote_api"

describe IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::IdsApi do
  let(:host){"http://api.nbos.io"}
  let(:ids_api){IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::IdsApi.new(host)}

  it 'should be the subclass of NetworkApi' do
    expect(ids_api).to be_a_kind_of(IdnSdkRuby::Com::Nbos::Capi::Api::V0::NetworkApi)
  end

  it 'should set host' do
    expect(ids_api.getHost).not_to be_nil
    expect(ids_api.getHost).to eq(host)
  end

  it 'should set remote Api class as IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::IdsRemoteApi' do
    expect(ids_api.getRemoteApiClass).to be IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::IdsRemoteApi
  end


end