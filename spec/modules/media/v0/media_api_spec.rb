require 'spec_helper'

describe IdnSdkRuby::Com::Nbos::Capi::Modules::Media::V0::MediaApi do
   before(:each) do
     app_context = double('IdsRailsApiContext')
     allow(app_context).to receive(:getClientCredentials){{"client_id"=>"sample-app-client",
                                                           "client_secret"=>"sample-app-secret"}}
     allow(app_context).to receive(:getName) {'app'}
     allow(app_context).to receive(:init)
     allow(app_context).to receive(:name){'app'}
     allow(app_context).to receive(:getHost).with(anything()){'http://api.qa1.nbos.io'}
     allow(app_context).to receive(:setClientToken).with(anything()){anything()}
     allow(app_context).to receive(:getUserToken).with("identity"){"identitytoken"}
     IdnSdkRuby::Com::Nbos::Capi::Api::V0::IdnSDK.init(app_context)
   end

  it 'should fetch Profile Picture from NBOS Server' do
    media_api = IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::Ids.getModuleApi("media")
    response = media_api.getMedia("mrd:123", "profile")
    expect(response[:status]).to eq(200)
    expect(response[:data]).to be_a_kind_of(IdnSdkRuby::Com::Nbos::Capi::Modules::Media::V0::MediaApiModel)
  end

  it 'should upload Picture from NBOS Server' do
    media_api = IdnSdkRuby::Com::Nbos::Capi::Modules::Ids::V0::Ids.getModuleApi("media")
    media_path = File.expand_path('./', 'spec/fixtures/temp_user.png')
    response = media_api.uploadMedia("mrd:123", "profile", media_path)
    expect(response[:status]).to eq(200)
    expect(response[:data]).to be_a_kind_of(IdnSdkRuby::Com::Nbos::Capi::Modules::Media::V0::MediaApiModel)
  end
end