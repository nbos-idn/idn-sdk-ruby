require 'webmock/rspec'

WebMock.disable_net_connect!(:allow_localhost => true)
RSpec.configure do |config|
    config.before(:each) do
        WebMock.stub_request(:get, "http://api.qa1.nbos.io/oauth/token?client_id=sample-app-client&client_secret=sample-app-secret&grant_type=client_credentials&scope%5B%5D=").
            with(:headers => {'Accept'=>'application/json', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body => {"access_token": "5d70771a-ee14-42d9-a5fa-e62e8d24a446",
                                                "token_type":"bearer","expires_in":272217}.to_s, :headers => {})


        media_api_response = {"extension"=>"png", "mediaFileDetailsList"=>[{"mediapath"=>"http://api.qa1.nbos.io/Media/default/default-profile_48x48.png",
                                                       "mediatype"=>"small"},
                                                      {"mediapath"=>"http://api.qa1.nbos.io/Media/default/default-profile_300x200.png",
                                                       "mediatype"=>"medium"},
                                                      {"mediapath"=>"http://api.qa1.nbos.io/Media/default/default-profile.png",
                                                       "mediatype"=>"original"}],
                              "supportedsizes"=>"small:48x48,medium:300x200", "uuid"=>nil}

        sign_up_response =   {"member"=>{"description"=>"", "email"=>"jagadishwerb+1@wavelabs.in",
                                       "emailConnects"=>[], "firstName"=>"jane", "id"=>462,
                                       "isExternal"=>false, "jsonAttributes"=>"",
                                       "lastName"=>"doe", "phone"=>"", "socialAccounts"=>[],
                                       "uuid"=>"MBR:test-uuid"},
                              "token"=>{"access_token"=>"5be2373e-497c-4db8-be26-1b208d2b77c2",
                                      "expires_in"=>7775999,
                                      "refresh_token"=>"36edfbd7-796a-4582-b871-4af741e50e92",
                                      "scope"=>"", "token_type"=>"bearer"}}
        fb_connect_response = {"member"=>{"description"=>"", "email"=>"", "emailConnects"=>[],
                                          "firstName"=>"jane doe", "id"=>461,
                                          "isExternal"=>true, "jsonAttributes"=>"",
                                          "lastName"=>"", "phone"=>"",
                                          "socialAccounts"=>[{"accessToken"=>"Test::AccessToken",
                                                              "email"=>nil, "id"=>130, "imageUrl"=>"https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/13707769_1790378461192849_5526286606336782680_n.jpg?oh=0769cf07b5b54c1b5bd2d78eb35986bd&oe=592A9FD7",
                                                              "socialType"=>"facebook"}], "uuid"=>"MBR:test-uuid"},
                               "token"=>{"access_token"=>"Test::AccessToken", "expires_in"=>7775999,
                                         "refresh_token"=>"Test::RefreshToken", "scope"=>"",
                                         "token_type"=>"bearer"}}

        password_change_response = {"messageCode"=>"password.changed",
                                    "message"=>"password.changed"}

        logout_change_response = {"messageCode"=>"member.logout.success",
                                  "message"=>"member.logout.success"}

        WebMock.stub_request(:get, "http://api.qa1.nbos.io/api/media/v0/media?id=mrd:123&mediafor=profile").
            with(:headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer identitytoken', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body =>media_api_response.to_json, :headers => {"Content-Type"=> "application/json"})

        WebMock.stub_request(:post, "http://api.qa1.nbos.io/api/media/v0/media?id=mrd:123&mediafor=profile").
            with(:headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer identitytoken','Content-Length'=>'14571', 'Content-Type'=>'multipart/form-data; boundary=-----------RubyMultipartPost'}).
            to_return(:status => 200, :body => media_api_response.to_json, :headers => {"Content-Type"=> "application/json"})

        WebMock.stub_request(:post, "http://api.qa1.nbos.io/api/identity/v0/users/signup").
            with(:body => "{\"username\"=>\"jagadish2\", \"password\"=>\"654321\", \"email\"=>\"jagadishwerb+1@wavelabs.in\", \"firstName\"=>\"jane\", \"lastName\"=>\"doe\"}",
                 :headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer access_token', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body => sign_up_response.to_json,
                      :headers => {"Content-Type"=> "application/json"})

        WebMock.stub_request(:post, "http://api.qa1.nbos.io/api/identity/v0/auth/login").
            with(:body => "{\"username\":\"jagadish2\",\"password\":\"123456\",\"messageCode\":null,\"message\":null}",
                 :headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer access_token', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body => sign_up_response.to_json, :headers => {"Content-Type"=> "application/json"})

        WebMock.stub_request(:put, "http://api.qa1.nbos.io/api/identity/v0/users/MBR:test-uuid").
            with(:body => "{\"username\"=>\"jagadish2\", \"password\"=>\"654321\", \"email\"=>\"jagadishwerb+1@wavelabs.in\", \"firstName\"=>\"jane\", \"lastName\"=>\"doe\"}",
                 :headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer identitytoken', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body => sign_up_response["member"].to_json,
                      :headers => {"Content-Type"=> "application/json"})

        WebMock.stub_request(:post, "http://api.qa1.nbos.io/api/identity/v0/auth/changePassword").
            with(:body => "{\"password\":\"123456\",\"newPassword\":\"654321\",\"message\":null}",
                 :headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer identitytoken', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body => password_change_response.to_json,
                      :headers => {"Content-Type"=> "application/json"})

        WebMock.stub_request(:get, "http://api.qa1.nbos.io/api/identity/v0/auth/logout").
            with(:headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer identitytoken', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body => logout_change_response.to_json,
                      :headers => {"Content-Type"=> "application/json"})

        WebMock.stub_request(:post, "http://api.qa1.nbos.io/api/identity/v0/auth/social/facebook/connect").
            with(:body => "{\"clientId\":\"sample-app-client\",\"accessToken\":\"test::omniauth::token\",\"expiresIn\":\"1494312526\"}",
                 :headers => {'Accept'=>'application/json', 'Authorization'=>'Bearer access_token', 'Content-Type'=>'application/json'}).
            to_return(:status => 200, :body => fb_connect_response.to_json, :headers => {"Content-Type"=> "application/json"})

    end
end