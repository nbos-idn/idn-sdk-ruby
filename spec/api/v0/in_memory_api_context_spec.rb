require 'spec_helper'

describe IdnSdkRuby::Com::Nbos::Capi::Api::V0::InMemoryApiContext do

  let(:module_name){'app'}
  let(:in_memory_api_context){IdnSdkRuby::Com::Nbos::Capi::Api::V0::InMemoryApiContext.new(module_name)}
  let(:credential_map){{"client_id"=>"sample-app-client", "client_secret"=>"sample-app-secret"}}
  let(:tokenApiModel){ double('TokenApiModel')}
  let(:host){ "http://api.qa.nbos.io"}

  it 'should be a sub class of IdnSdkRuby::Com::Nbos::Capi::Api::V0::AbstractApiContext' do
    expect(in_memory_api_context.kind_of?(IdnSdkRuby::Com::Nbos::Capi::Api::V0::AbstractApiContext)).to be true
  end

  it 'should respond to getname methods' do
    expect(in_memory_api_context.getName).to be module_name
  end

  it 'should respond to getClientCredentials methods' do
    in_memory_api_context.setClientCredentials(credential_map)
    expect(in_memory_api_context.getClientCredentials).to be credential_map
  end

  it 'should respond to getClientToken methods' do
     in_memory_api_context.setClientToken(tokenApiModel)
     expect(in_memory_api_context.getClientToken).to be tokenApiModel
  end

  it 'should respond to getUserToken methods' do
    in_memory_api_context.setUserToken(module_name, tokenApiModel)
    expect(in_memory_api_context.getUserToken(module_name)).to be tokenApiModel

  end

  it 'should respond to getHost methods' do
    in_memory_api_context.setHost(module_name, host)
    expect(in_memory_api_context.getHost(module_name)).to be host
  end

end