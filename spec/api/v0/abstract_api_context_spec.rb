require 'spec_helper'

describe IdnSdkRuby::Com::Nbos::Capi::Api::V0::AbstractApiContext do

  let(:name){'app'}
  let(:api_context) do
    api_context = double('IdsRailsApiContext')
    allow(api_context).to receive(:getClientCredentials) { {"client_id"=>"sample-app-client", "client_secret"=>"sample-app-secret"}}
    allow(api_context).to receive(:getName) {'app'}
    allow(api_context).to receive(:init)
    allow(api_context).to receive(:name){'app'}
    allow(api_context).to receive(:getHost).with(anything()){'http://api.qa1.nbos.io'}
    allow(api_context).to receive(:setClientToken).with(anything()){anything()}
    api_context
  end

  it 'should return Name of the Context as one specified during initialization' do
    abstract_api_context = IdnSdkRuby::Com::Nbos::Capi::Api::V0::AbstractApiContext.new(name)
    expect(abstract_api_context.name).to be name
  end

  it 'should return Default Name for a Context, if no name is givem' do
    abstract_api_context = IdnSdkRuby::Com::Nbos::Capi::Api::V0::AbstractApiContext.new()
    expect(abstract_api_context.name).to eq 'app'
  end

  it 'should register api context' do
    IdnSdkRuby::Com::Nbos::Capi::Api::V0::AbstractApiContext.registerApiContext(api_context)
    expect(IdnSdkRuby::Com::Nbos::Capi::Api::V0::AbstractApiContext.get(name)).to be api_context
  end

end