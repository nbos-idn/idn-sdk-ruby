require 'spec_helper'

describe  IdnSdkRuby::Com::Nbos::Capi::Api::V0::TokenApiModel do
  let(:parsed_json){{"access_token"=> "5d70771a-ee14-42d9-a5fa-e62e8d24a446",
                     "refresh_token"=> "5d70771a-ee14-42d9-a5fa-e62e8d24a447",
                     "token_type"=> "bearer",
                     "scope"=>  "test",
                     "expires_in"=> "272217"}}

  let(:token){IdnSdkRuby::Com::Nbos::Capi::Api::V0::TokenApiModel.new(parsed_json)}

  it 'should return Token expiration time' do
    expect(token.getExpires_in).to be parsed_json["expires_in"]
  end
  it 'should return Token Scope' do
    expect(token.getScope).to be parsed_json["scope"]
  end

  it 'should return Access Token' do
    expect(token.getToken_type).to be parsed_json["token_type"]
  end

  it 'should return Access Token'do
    expect(token.getAccess_token).to be parsed_json["access_token"]
  end

  it 'should return refresh Token' do
    expect(token.getRefresh_token).to be parsed_json["refresh_token"]
  end

  it 'should return as Json Object' do
    expect(token.as_json.class).to be Hash
  end

end