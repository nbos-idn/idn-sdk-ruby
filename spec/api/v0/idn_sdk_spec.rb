require 'spec_helper'

describe IdnSdkRuby do

  it 'should returns token object for given App Context' do
    app_context = double('IdsRailsApiContext')
    allow(app_context).to receive(:getClientCredentials) { {"client_id"=>"sample-app-client", "client_secret"=>"sample-app-secret"}}
    allow(app_context).to receive(:getName) {'app'}
    allow(app_context).to receive(:init)
    allow(app_context).to receive(:name){'app'}
    allow(app_context).to receive(:getHost).with(anything()){'http://api.qa1.nbos.io'}
    allow(app_context).to receive(:setClientToken).with(anything()){anything()}
    expect(IdnSdkRuby::Com::Nbos::Capi::Api::V0::IdnSDK.init(app_context)).to be_kind_of IdnSdkRuby::Com::Nbos::Capi::Api::V0::TokenApiModel
  end
end
